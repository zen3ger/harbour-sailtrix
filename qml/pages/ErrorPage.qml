import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    allowedOrientations: Orientation.All

    property string errorMsg;

    PageHeader {
        title: qsTr("Error")
    }

    Column {
        id: column

        width: page.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: Theme.paddingLarge



        Label {
            text: "Error"
            color: Theme.errorColor
            font.family: Theme.fontFamilyHeading
            font.pixelSize: Theme.fontSizeLarge
            anchors.horizontalCenter: parent.horizontalCenter
        }


        Label {
            text: errorMsg
            color: Theme.errorColor
            width: parent.width
            leftPadding: Theme.horizontalPageMargin
            rightPadding: Theme.horizontalPageMargin
            wrapMode: "WrapAtWordBoundaryOrAnywhere"
        }
    }
}
