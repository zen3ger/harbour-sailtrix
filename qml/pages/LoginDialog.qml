import QtQuick 2.0
import Sailfish.Silica 1.0
import LoginBridge 1.0

Dialog {
    id: loginDialog

    allowedOrientations: Orientation.All
    onAccepted: {
        bridge.homeserverUrl = homeserverUrl.text
        bridge.username = username.text
        bridge.password = password.text
        bridge.deviceName = displayName.text
        bridge.login()
        pageStack.push("LoginWaiting.qml", { lBridge: bridge });
    }

    LoginBridge {
        id: bridge
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height
        contentWidth: column.width
        Column {
            id: column

            width: page.width
            height: page.height
            spacing: Theme.paddingLarge

            DialogHeader {
                title: qsTr("Login")
            }

            TextField {
                label: qsTr("Homeserver")
                placeholderText: qsTr("Homeserver URL (matrix.org)")
                width: parent.width
                id: homeserverUrl
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: username.focus = true
                validator: RegExpValidator { regExp: /^((?!-))(xn--)?[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.(xn--)?([a-zA-Z0-9\-]{1,61}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,})$/ }
            }

            TextField {
                label: qsTr("Username")
                placeholderText: qsTr("Username (user1)")
                width: parent.width
                id: username
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: password.focus = true
                validator: RegExpValidator { regExp: /^[^@:]*$/ }
            }

            PasswordField {
                label: qsTr("Password")
                width: parent.width
                id: password
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {
                    bridge.homeserverUrl = homeserverUrl.text
                    bridge.username = username.text
                    bridge.password = password.text
                    bridge.deviceName = displayName.text
                    bridge.login()
                    pageStack.push("LoginWaiting.qml", { lBridge: bridge });
                }
            }

            TextField {
                label: qsTr("Device display name");
                placeholderText: "Sailtrix on SailfishOS"
                width: parent.width
                id: displayName
                text: "Sailtrix on SailfishOS"
            }
        }
    }
}
