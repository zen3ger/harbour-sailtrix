import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0
import LoginBridge 1.0

Page {
    id: loginPage

    allowedOrientations: Orientation.All


    LoginBridge {
        id: bridge

        onDiscovered: {
            webviewer.url = bridge.homeserverUrl + "/_matrix/client/r0/login/sso/redirect?redirectUrl=https://0.0.0.0/sailtrixSSO"
            webviewer.visible = true
            indicator.running = false
        }

        onErrorChanged: {
            if (error == "Done") {
                pageStack.clear();
                pageStack.replace("Rooms.qml")
            }
        }
    }

    Column {
        id: column

        width: parent.width
        spacing: Theme.paddingLarge

        PageHeader {
            title: qsTr("Login with SSO")
        }

        TextField {
            label: qsTr("Homeserver")
            placeholderText: qsTr("Homeserver URL (matrix.org)")
            width: parent.width
            id: homeserverUrl

            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.enabled: text.length > 0
            EnterKey.onClicked: {
                bridge.homeserverUrl = homeserverUrl.text;
                bridge.login(true);
                indicator.running = true
                column.visible = false
            }
        }

        Button {
            text: "Continue"

            id: continueButton


            preferredWidth: Theme.buttonWidthLarge
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: {
                bridge.homeserverUrl = homeserverUrl.text;
                bridge.login(true);
                indicator.running = true
                column.visible = false
            }
        }
    }


    WebView {
        visible: false
        id: webviewer
        anchors.fill: parent

        onUrlChanged: {
            if (url.toString().lastIndexOf("https://0.0.0.0/sailtrixSSO") === 0) {
                bridge.ssoLogin(url.toString().substring(39));
                webviewer.visible = false
                indicator.running = true
            }
        }
    }

    PageBusyIndicator {
        id: indicator
    }
}
