#include <QtQuick>

#include <sailfishapp.h>
#include <QStandardPaths>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <Sailfish/Secrets/secretmanager.h>

#include "loginbridge.h"
#include "rooms.h"
#include "messages.h"
#include "settingsbackend.h"
#include "messages.h"
#include "userbackend.h"
#include "picturedisplaybackend.h"
#include "invitebackend.h"
#include "roomdirectorybackend.h"
#include "roomdirectorymodel.h"
#include "createroombackend.h"
#include "dbus/dbusactivation.h"
#include "dbus/sailtrixsignals.h"
#include "olm/olm.h"
#include "enc-util.h"

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/Sailtrix.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device)


    QString url;

    if (argc > 1) {
        QString first_arg(argv[1]);
        qDebug() << "First argument:" << first_arg;
        if (first_arg == QStringLiteral("--reset")) {
            Sailfish::Secrets::SecretManager m_secretManager;
            delete_collection(&m_secretManager, QLatin1String("SailtrixWallet2"));

            QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix");
            cache.removeRecursively();


            QDir config(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/org.yeheng/sailtrix");
            cache.removeRecursively();

            qInfo() << "Reset wallet";
            return 0;
        } else if (first_arg.startsWith("http:") || first_arg.startsWith("https:") || first_arg.startsWith("matrix:")) {
            qDebug() << "Launching URL:" << first_arg;
            url = first_arg;
        }
    }

    QDBusConnection conn = QDBusConnection::sessionBus();
    if (conn.interface()->isServiceRegistered("org.yeheng.sailtrix")) {
        // open up other instance of app
        qDebug() << "Opening existing instance";
        if (url.isEmpty() || url.isNull()) {
            return DBusActivation::sendOpenSignal() ? 0 : 1;
        } else {
            return DBusActivation::sendOpenSignalWithUrl(url) ? 0 : 1;
        }
    }

    qmlRegisterType<LoginBridge>("LoginBridge", 1, 0, "LoginBridge");
    qmlRegisterType<Rooms>("RoomsBackend", 1, 0, "RoomsBackend");
    // qmlRegisterType<Messages>("Messages", 1, 0, "Messages");
    qmlRegisterType<SettingsBackend>("SettingsBackend", 1, 0, "SettingsBackend");
    qmlRegisterType<UserBackend> ("UserBackend", 1, 0, "UserBackend");
    qmlRegisterType<PictureDisplayBackend>("PictureDisplayBackend", 1, 0, "PictureDisplayBackend");
    qmlRegisterType<InviteBackend>("InviteBackend", 1, 0, "InviteBackend");
    qmlRegisterType<RoomDirectoryBackend>("RoomDirectoryBackend", 1, 0, "RoomDirectoryBackend");
    qmlRegisterType<CreateRoomBackend>("CreateRoomBackend", 1, 0, "CreateRoomBackend");
    qmlRegisterType<DBusActivation>("DBusActivation", 1, 0, "DBusActivation");
    qmlRegisterType<SailtrixSignals>("SailtrixSignals", 1, 0, "SailtrixSignals");

    qmlRegisterType<Messages>("Messages", 1, 0, "Messages");
    qRegisterMetaType<RoomsModel*>("RoomsModel*");
    qRegisterMetaType<MessagesModel*>("MessagesModel*");
    qRegisterMetaType<MessagesModel*>("MessagesModel*");
    qRegisterMetaType<RoomDirectoryModel*>("RoomDirectoryModel*");



    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    app->setOrganizationName(QStringLiteral("org.yeheng"));
    app->setApplicationName(QStringLiteral("sailtrix"));
    app->setQuitOnLastWindowClosed(false);


    QScopedPointer<DBusActivation> dbus(new DBusActivation(app.data()));
    if (url.isNull() || url.isEmpty())
        dbus->sendOpenSignal();
    else
        dbus->sendOpenSignalWithUrl(url);

    qDebug() << "Starting sailtrix...";

    return app->exec();
}
