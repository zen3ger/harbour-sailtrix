#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QTimer>
#include <QMap>
#include <QString>
#include <QSet>
#include <QJsonDocument>
#include <Sailfish/Secrets/secretmanager.h>
#include <keepalive/backgroundactivity.h>
#include <nemonotifications-qt5/notification.h>
#include "sailtrixsignals.h"

class Notifications : public QObject
{
    Q_OBJECT
public:
    explicit Notifications(QObject *parent = nullptr, SailtrixSignals* _sig = nullptr);
    ~Notifications();
    void pause();
    void resume();
    bool isStopped();
signals:
private:
    Sailfish::Secrets::SecretManager m_secretManager;
    QString m_hs_url;
    QString m_access_token;
    QNetworkAccessManager* manager;
    BackgroundActivity* activity;
    SailtrixSignals* sig;
    QString next;
    qint64 start_time;
    BackgroundActivity::Frequency freq = BackgroundActivity::ThirtySeconds;
    bool m_disabled = false;
    QMap<QString, Notification*> notifications_map;
    QSet<Notification*> new_notifications;
    QJsonDocument rooms_cache;
    QDateTime rooms_cache_time;
    QJsonDocument users_cache;
    QDateTime users_cache_time;
public slots:
    void disable();
    void enable();
    void changeFrequency(int new_freq);
private slots:
    void startRun();
    void processNotifications(QNetworkReply* reply);

};

#endif // NOTIFICATIONS_H
