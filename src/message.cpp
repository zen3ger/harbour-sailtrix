#include "message.h"

Message::Message(QString display_name, QString user_id, QString content, QString avatar_mxc, QString avatar) :
    m_display_name { display_name },
    m_user_id { user_id },
    m_content { content },
    m_avatar_mxc { avatar_mxc },
    m_avatar { avatar } {};

QString Message::display_name() { return m_display_name; }
QString Message::user_id() { return m_user_id; }
QString Message::content() { return m_content; }
QString Message::avatar_mxc() { return m_avatar_mxc; }
QString Message::avatar() { return m_avatar; }

void Message::set_display_name(QString &display_name) {
    if (display_name == m_display_name) return;
    m_display_name = display_name;
}

void Message::set_user_id(QString &user_id) {
    if (user_id == m_user_id) return;
    m_user_id = user_id;
}

void Message::set_content(QString &content) {
    if (content == m_content) return;
    m_content = content;
}

void Message::set_avatar_mxc(QString &avatar_mxc) {
    if (avatar_mxc == m_avatar_mxc) return;
    m_avatar_mxc = avatar_mxc;
}

void Message::set_avatar(QString &avatar) {
    if (avatar == m_avatar) return;
    m_avatar = avatar;
}
