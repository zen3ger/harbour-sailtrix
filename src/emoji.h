#ifndef EMOJI_H
#define EMOJI_H
#include <QStringList>

const QStringList SAS_EMOJIS = {"🐶", "🐱", "🦁", "🐎", "🦄", "🐷", "🐘", "🐰", "🐼", "🐓", "🐧", "🐢", "🐟", "🐙", "🦋", "🌷", "🌳", "🌵", "🍄", "🌏", "🌙", "☁️", "🔥", "🍌", "🍎", "🍓", "🌽", "🍕", "🎂", "❤️", "😀", "🤖", "🎩", "👓", "🔧", "🎅", "👍", "☂️", "⌛", "⏰", "🎁", "💡", "📕", "✏️", "📎", "✂️", "🔒", "🔑", "🔨", "☎️", "🏁", "🚂", "🚲", "✈️", "🚀", "🏆", "⚽", "🎸", "🎺", "🔔", "⚓", "🎧", "📁", "📌"};
const QStringList SAS_EMOJI_TEXT = {"Dog", "Cat", "Lion", "Horse", "Unicorn", "Pig", "Elephant", "Rabbit", "Panda", "Rooster", "Penguin", "Turtle", "Fish", "Octopus", "Butterfly", "Flower", "Tree", "Cactus", "Mushroom", "Globe", "Moon", "Cloud", "Fire", "Banana", "Apple", "Strawberry", "Corn", "Pizza", "Cake", "Heart", "Smiley", "Robot", "Hat", "Glasses", "Spanner", "Santa", "Thumbs Up", "Umbrella", "Hourglass", "Clock", "Gift", "Light Bulb", "Book", "Pencil", "Paperclip", "Scissors", "Lock", "Key", "Hammer", "Telephone", "Flag", "Train", "Bicycle", "Aeroplane", "Rocket", "Trophy", "Ball", "Guitar", "Trumpet", "Bell", "Anchor", "Headphones", "Folder", "Pin"};

#endif // EMOJI_H
