#ifndef PICTUREDISPLAYBACKEND_H
#define PICTUREDISPLAYBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>
#include <Sailfish/Secrets/secretmanager.h>


class PictureDisplayBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path NOTIFY loaded)
public:
    explicit PictureDisplayBackend(QObject *parent = nullptr);
    ~PictureDisplayBackend();
    Q_INVOKABLE void load(QString mxc);
    Q_INVOKABLE void save(QString file_name);
    QString path();
signals:
    void loaded();
    void image_saved();
    void image_error();
private:
    QNetworkAccessManager* manager;
    QString p;
    Sailfish::Secrets::SecretManager m_secretManager;
private slots:
    void process(QNetworkReply* reply);
};

#endif // PICTUREDISPLAYBACKEND_H
